from random import randint, random


def main():
    people = randomPeople()
    print(people)

    sortedPeople = sortByAge(people)
    print(sortedPeople)


def randomPeople():
    people = []
    for i in range(10):
        people.append({'id': i, 'age': randint(1, 100)})
    return people


def sortByAge(people):
    sortedPeople = []
    for p1 in people:
        maxP = {'age': 0}
        for p2 in people:
            if p2['age'] > maxP['age'] and p2 not in sortedPeople:
                maxP = p2
        sortedPeople.append(maxP)

    print(sortedPeople[len(sortedPeople)-1]['id'])
    print(sortedPeople[0]['id'])

    return sortedPeople


if __name__ == "__main__":
    main()
