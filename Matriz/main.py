from random import randint


def main():
    matrix = createMatrix5x5()
    show(matrix)
    print()
    listOfSecuences(matrix)



def IsConsecutiveSecuence(secuence):
    consecutive = True
    for i in range(len(secuence)):
        if i == 0:
            continue
        consecutive = consecutive and secuence[i] == secuence[i-1]-1
    return consecutive


def listOfSecuences(matrix):
    # j -> horizontal
    # i -> vertical
    secuenceLenght = 4
    consecutivesSecuences = []
    for i in range(len(matrix[0])):
        for j in range(len(matrix)):
            # Pivots of secuence

            # Secuencia hacia la derecha
            if j <= len(matrix) - secuenceLenght:
                secuence = horizontalSecuence(j, i, secuenceLenght, matrix)
                if not IsConsecutiveSecuence(secuence):
                    if not IsConsecutiveSecuence(secuence[::-1]):
                        continue

                print(secuence)
                print('X: ',j ,'Y: ', i)
                print('X: ',j+secuenceLenght ,'Y: ', i)
                consecutivesSecuences.append(secuence)

            # Secuencia hacia abajo
            if i <= len(matrix) - secuenceLenght:
                secuence = verticalSecuence(j, i, secuenceLenght, matrix)
                if not IsConsecutiveSecuence(secuence):
                    if not IsConsecutiveSecuence(secuence[::-1]):
                        continue
                    
                print(secuence)
                print('X: ',j ,'Y: ', i)
                print('X: ',j ,'Y: ', i+secuenceLenght)
                consecutivesSecuences.append(secuence)

    return consecutivesSecuences

def horizontalSecuence(x, y, secuenceLenght, matrix):
    secuence = []
    for i in range(secuenceLenght):
        secuence.append(matrix[x+i][y])
    return secuence


def verticalSecuence(x, y, secuenceLenght, matrix):
    secuence = []
    for i in range(secuenceLenght):
        secuence.append(matrix[x][y+i])
    return secuence


def createMatrix5x5():
    matrix = []
    for i in range(5):
        matrix.append(randomList(5))
    return matrix


def randomList(cant):
    list = []
    for i in range(cant):
        list.append(randint(0, 9))
    return list


def show(matrix):
    for i in range(len(matrix[0])):
        for j in matrix:
            print(j[i], end=' ')
        print()
    # 1 2 3
    # 4 5 6
    # 7 8 9
    # print order


if __name__ == "__main__":
    main()
