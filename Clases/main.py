from circulo import Circulo


def main():
    circulo = Circulo(5)
    print(circulo)
    circulo.radio = -5
    print(circulo*-2)
    print(circulo*4)


if __name__ == "__main__":
    main()
