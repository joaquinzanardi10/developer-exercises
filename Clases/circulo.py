import math


class Circulo:

    def __init__(self, radio):
        try:
            if radio <= 0:
                raise ValueError
            self._radio = radio
        except ValueError:
            print(
                'No se puede crear un circulo con radio negativo o 0')

    def area(self):
        return math.pi * (self.radio ** 2)

    def perimetro(self):
        return (2 * math.pi * self.radio)

    def __mul__(self, n):
        try:
            if n <= 0:
                raise ValueError
            return Circulo(self.radio*n)
        except ValueError:
            print(
                'No se puede multiplicar un circulo con un numero negativo')
        

    @property
    def radio(self):
        return self._radio

    @radio.setter
    def radio(self, radio):
        try:
            if radio <= 0:
                raise ValueError
            self._radio = radio
        except ValueError:
            print(
                'No se puede modificar el radio de un circulo a un numero negativo o 0')

    def __str__(self):
        return ("Circulo de radio " + str(self._radio))

